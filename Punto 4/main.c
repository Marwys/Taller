#include <xc.h>


#pragma config FOSC = HS 

#pragma config WDTE = OFF 

#pragma config PWRTE = OFF 

#pragma config MCLRE = ON

#pragma config CP = OFF 

#pragma config CPD = OFF 

#pragma config BOREN = OFF 

#pragma config IESO = OFF

#pragma config FCMEN = OFF

#pragma config LVP = OFF 

#pragma config DEBUG = OFF 


#define _XTAL_FREQ 20000000

#define TIEMPO 40

#define DELAY 1000



const unsigned char DIGITOS[10] = {0x3F, 0x06,  0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};


void mostrar_numeros(int valor)

{

    unsigned char aux = 0;

    unsigned char dec= 0;

    unsigned char uni = 0;

    
    aux = valor/100; 

    dec = (valor-aux*100)/10; 

    uni = valor-aux*100-dec*10; 

    
    PORTC = DIGITOS[uni];

    PORTD = 0x02;

    __delay_ms(TIEMPO);

    PORTD = 0x00;


    PORTC = DIGITOS[dec];

    PORTD = 0x01;

    __delay_ms(TIEMPO);

    PORTD = 0x00;

}


int main(void)

{

    unsigned char cont = 0;
    
    unsigned int aux = 0;

    int valor = 0;

    
    PORTC = 0x00;

    TRISC = 0x00;

    
    PORTD = 0x00;

    TRISD = 0x3C;


    while(1)

    {

        mostrar_numeros(valor);

        if(RD2 == 0){
            __delay_ms(DELAY);
            if(RD2 == 0){
                aux = 1;
            }
        }
        
        if(RD3 == 0){
            __delay_ms(DELAY);
            if(RD3 == 0){
                aux = 2;
            }
        }
        
        if(RD4 == 0){
            __delay_ms(DELAY);
            if(RD4 == 0){
                aux = 1;
            }
        }
        
        if(RD5 == 0){
            __delay_ms(DELAY);
            if(RD5 == 0){
                aux = 2;
                valor = 0;
                cont = 0;
            }
        }

        if(aux == 1){
            cont++;
            
            if(cont == 12)

        {

            cont = 0;

            valor++;

            
            if(valor == 100)

            {

                valor = 0;

            }

        }

    }
        }

    
    return 0;

    
}
